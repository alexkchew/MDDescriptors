# WELCOME TO MDDESCRIPTORS!

The goal of this project is to develop MDDescriptors that we can share with each other and share analysis tools that could be applied more broadly for extracting important features derived from molecular dynamics simulations. This code is designed to allow transferable codes that is used across many simulation systems (e.g. radial distribution functions, hydrogen bonding, water analysis, etc.)

# Description of files
- `application`: folder containing descriptors for specific application areas
- `core`: folder that contains all core variables and functions
- `geometry`: [DEPRECIATED] folder that contains radial distribution functions, etc.
- `global_vars`: codes that contain all global variables
- `parallel`: codes that enable Python parallel code
- `publishing_tools`: folder containing publishing tools when we have everything finalized
- `surface`: folder containing methods for computing surface information (e.g. Willard-Chandler interface)
- `templates`: [DEPRECIATED] folder containing examples of template code
- `traj_tools`: codes that are used to analyze MD simulations
- `visualization`: codes to visualize 3D spatial distribution maps

