application: contains all application specific code

Folder description:
	bilayer: contains all scripts for lipid bilayers
	hydrophobicity: contains all scripts to measure hydrophobicity
	nanoparticle: contains all scripts for nanoparticle design
	solvent_effects: contains all scripts for understanding solvent effects in acid-catalyzed reactions