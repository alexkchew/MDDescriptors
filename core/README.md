# DESCRIPTION
This folder contains all scripts that is applicable for all users. Please only include scripts here that is broadly applicable. Application specific scripts should be placed in MDDescriptors > application.

# LIST OF SCRIPTS
- calc_tools: contains calculation tools (e.g. center of mass calculation)
- check_tools: contains scripts to check variables (e.g. checks if input variables for a class is correct)
- import_tools: contain scripts to import files (e.g. importing trajectory using MDTraj)
- decoder: contains scripts for decoding directory names (e.g. interpretation of the directory name you are in)
- general_traj_info: contains scripts that are generally used for trajectories to extract information, such as box length, volume, etc.
- initialize: contains scripts that are required for running your python correctly (e.g. correcting directory path when running on the SWARM server)
- plot_tools: contains scripts for plotting purposes
- read_write_tools: contains scripts to read gro, top, and itp files
